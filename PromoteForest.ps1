﻿$DomainName = Read-Host "Enter New Domain Name"
$DomainNetBIOSName = Read-Host "Enter New Domain NetBIOS Name"

# INSTALL ADDC ROLE ON SERVER
Install-WindowsFeature -name AD-Domain-Services -IncludeManagementTools

# INSTALL FOREST AND DOMAIN CONTROLLER
Install-ADDSForest -DomainName $DomainName -DomainNetBIOSName $DomainNetBIOSName -InstallDNS